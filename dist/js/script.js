/* global monogatari */

// Define the messages used in the game.
monogatari.action ('message').messages ({
	'config': {
		title: 'Konfiguracja hiperparametrów...',
		body: `
			<p>Drogi Graczu,<br /> za chwilę zostanie ci zadanych kilka pytań.<br />
			Twoje odpowiedzi wpłyną na przebieg rozgrywki, zatem wybieraj uważnie. </p>
		`
	},
	'postConfig': {
		title: 'Konfiguracja zakończona!',
		body: `
			<p>Twoje odpowiedzi zostały zapisane. Pamiętaj, że w każdej chwili możesz wrócić i je zmienić. Wpłynie to jednak na przebieg rozgrywki. Będzie również jednoznaczne z porzuceniem relacji, którą udało ci się zbudować. </p>
		`
	}
});

// Define the notifications used in the game
monogatari.action ('notification').notifications ({
	'Welcome': {
		title: 'Welcome',
		body: 'This is the Monogatari VN Engine',
		icon: ''
	}
});

// Credits of the people involved in the creation of this awesome game
monogatari.configuration ('credits', {

});

// Define the Particles JS Configurations used in the game
monogatari.action ('particles').particles ({

});

// Define the images that will be available on your game's image gallery
monogatari.assets ('gallery', {

});

// Define the music used in the game.
monogatari.assets ('music', {
	// deepBlue: 'bensound-deepblue.mp3',
	// acousticBreeze: 'bensound-acousticbreeze.mp3',
	// scifi: 'bensound-scifi.mp3',
	// memories: 'bensound-memories.mp3',
	// sadDay: 'bensound-sadday.mp3',
	// tomorrow: 'bensound-tomorrow.mp3',
	// enigmatic: 'bensound-enigmatic.mp3',
	// psychedelic: 'bensound-psychedelic.mp3',
	// retrosoul: 'bensound-retrosoul.mp3',
	// hipjazz: 'bensound-hipjazz.mp3',
	// hipjazz_EQ: 'hip jazz_EQ.mp3',
	// retrosoul_EQ: 'retro soul_EQ.mp3',
	// Magical_Dirt: 'Magical_Dirt.mp3',
	// White_Hats: 'White_Hats.mp3',
	// creepy: 'bensound-creepy.mp3',
	// insects: 'insects.mp3',
	// evolution: 'bensound-evolution.mp3',
	// november: 'bensound-november.mp3',
	// piano_moment: 'bensound-pianomoment.mp3',
	// We_ll_Meet_Again: 'We_ll_Meet_Again.mp3',
	ending: 'ending.mp3'
});

// Define the voice files used in the game.
monogatari.assets ('voices', {});

// Define the sounds used in the game.
monogatari.assets ('sounds', {
	// loudKnock: 'loudKnock.mp3',
	// louderKnock: 'louderKnock.mp3',
	// softKnock: 'softKnock.mp3',
	// cameraShutter: 'cameraShutter.mp3',
	// notification: 'notification.mp3',
	// keyboardLogin: 'keyboardLogin.mp3',
	// slammingTheDoor: 'slammingTheDoor.mp3',
	// openingTheDoor: 'openingTheDoor.mp3',
	// slowDoorOpening: 'slowDoorOpen.mp3',
	// schoolBell: 'schoolBell.mp3',
	// thump: 'thump.mp3',
	// pulling: 'pulling.mp3',
	// laughTrack: 'laughTrack.mp3',
	// dentalDrill: 'dentalDrill.mp3',
	// dentalDrillLong: 'dentalDrillLong.mp3',
	// ruriri_007: 'ruriri_007.mp3',
	// Glass_Windows_Breaking: 'Glass_Windows_Breaking.mp3',
	// bodyfall: 'body fall.mp3',
	// bitpowerup: '8-bit-powerup.mp3',
	// screeching: 'screeching.mp3',
	// doorhandle: 'doorhandle.mp3',
	// schoolChildren: 'schoolChildren.mp3',
	// hospitalCorridor: 'hospitalCorridor.mp3',
	// dentalOffice: 'dentalOffice.mp3',
	// slidingDoor: 'slidingDoor.mp3',
	'blizzard': 'blizzard.mp3',
	'typing1': 'typing1.mp3',
	'typing2': 'typing2.mp3',
	'typing3': 'typing3.mp3',
	'typing4': 'typing4.mp3',
	'beach_sea': 'beach_sea.mp3',
	'street1' : 'street1.mp3',
	'street2' : 'street2.mp3',
	'beep' : 'beep.mp3'
});

// Define the videos used in the game.
monogatari.assets ('videos', {

});

// Define the images used in the game.
monogatari.assets ('images', {

});

function canUseWebP () {
	var elem = document.createElement('canvas');
	if (elem.getContext && elem.getContext('2d')) {
		return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
	}
	return false;
}
var webPSupported = canUseWebP();

// Define the backgrounds for each scene.
monogatari.assets ('scenes', {
	// bathroom: webPSupported ? 'bathroom.webp' : 'bathroom.png',
	// beachDaySimple: webPSupported ? 'beach_day_simple.webp' : 'beach_day_simple.png',
	// beachDayUmbrella: webPSupported ? 'beach_day_umbrella.webp' : 'beach_day_umbrella.png',
	// beachSunsetSimple: webPSupported ? 'beach_sunset_simple.webp' : 'beach_sunset_simple.png',
	// bedroomNight: webPSupported ? 'bedroom_night.webp' : 'bedroom_night.png',
	// bedroom: webPSupported ? 'bedroom.webp' : 'bedroom.png',
	// busStop: webPSupported ? 'bus_stop.webp' : 'bus_stop.png',
	// cafe: webPSupported ? 'cafe.webp' : 'cafe.png',
	// classroomEvening: webPSupported ? 'classroom_evening.webp' : 'classroom_evening.png',
	// classroom: webPSupported ? 'classroom.webp' : 'classroom.png',
	// classroomNight: 'classroom_night.png',
	// hallwayHouse: webPSupported ? 'hallway_house.webp' : 'hallway_house.jpg',
	// hallwayHouseNight: webPSupported ? 'hallway_house_night.webp' : 'hallway_house_night.png',
	// hallwaySchool: webPSupported ? 'hallway_school.webp' : 'hallway_school.png',
	// hallwaySchoolNight: 'hallway_school_night.png',
	// hallwaySchoolDarker30: 'hallway_school_darker_30.png',
	// hallwaySchoolDarker50: 'hallway_school_darker_50.png',
	// hallwaySchoolDarker60: 'hallway_school_darker_60.png',
	// hallwaySchoolDarker70: 'hallway_school_darker_70.png',
	// hallwaySchoolDarker75: 'hallway_school_darker_75.png',
	// hallwaySchoolDarker80: 'hallway_school_darker_80.png',
	// hallwaySchoolDarker90: 'hallway_school_darker_90.png',
	// hallway: webPSupported ? 'hallway.webp' : 'hallway.jpg',
	// hallway2: webPSupported ? 'hallway2.webp' : 'hallway2.jpg',
	// hallwayNight: webPSupported ? 'hallway_night.webp' : 'hallway_night.jpg',
	// housesEvening: webPSupported ? 'houses_evening.webp' : 'houses_evening.png',
	// housesNight: webPSupported ? 'houses_night.webp' : 'houses_night.png',
	// houses: webPSupported ? 'houses.webp' : 'houses.png',
	// japaneseSetting: webPSupported ? 'japanese_setting.webp' : 'japanese_setting.png',
	// kitchen: webPSupported ? 'kitchen.webp' : 'kitchen.png',
	// livingRoom: webPSupported ? 'living_room.webp' : 'living_room.png',
	// livingRoomNight: webPSupported ? 'living_room_night.webp' : 'living_room_night.png',
	// moonLinkFence: webPSupported ? 'moon_link_fence.webp' : 'moon_link_fence.png',
	// moonOverField: webPSupported ? 'moon_over_field.webp' : 'moon_over_field.png',
	// moonPark: webPSupported ? 'moon_park.webp' : 'moon_park.png',
	// moon: webPSupported ? 'moon.webp' : 'moon.png',
	// park: webPSupported ? 'park.webp' : 'park.png',
	// schoolCafeteria: webPSupported ? 'school_cafeteria.webp' : 'school_cafeteria.png',
	// schoolEntranceEvening: webPSupported ? 'school_entrance_evening.webp' : 'school_entrance_evening.png',
	// schoolEntrance: webPSupported ? 'school_entrance.webp' : 'school_entrance.png',
	// schoolRooftop: webPSupported ? 'school_rooftop.webp' : 'school_rooftop.png',
	// stairs: webPSupported ? 'stairs.webp' : 'stairs.png',
	// kitchenNight: webPSupported ? 'kitchen_night.webp' : 'kitchen_night.png',
	// end: webPSupported ? 'end.webp' : 'end.png',
	// Visuals 02 starts here
	// traditionalPavilion: 'traditional_pavilion.png',
	// traditionalPavilionNight: 'traditional_pavilion_night.png',
	// street1: 'street_1.png',
	// street_1Night: 'street_1Night.png',
	// street2: 'street_2.png',
	// shop1: 'shop_1.png',
	// resto: 'resto.png',
	// nightDistrict1: 'night_district_1.png',
	// nightDistrict2: 'night_district_2.png',
	// mountFuji: 'mount_fuji.png',
	// japaneseDinner: 'japanese_dinner.png',
	// houseNight: 'houseNight.png',
	// houseOld: 'house_old.png',
	// houseOldNight: 'house_old_night.png',
	// cityView: 'city_view.png',
	// time: 'time.jpg',
	// dentalOffice: 'dental_office.png',
	beach_back: 'beach_back.png',
	city: 'city.png',
	sea_night: 'sea_night.png',
	street_cafe: 'street_cafe.png',
	beach_road: 'beach_road.png',
	bunker: 'bunker.jpg',
});


// Define the Characters
monogatari.characters ({
	a: {
		name: 'Rin',
		color: '#68C5D9',
		directory: 'rin-aloes',
		sprites: {
			acpose1offended: 'acpose1offended.png',
			acpose1impatient: 'acpose1impatient.png',
			acpose1ironic: 'acpose1ironic.png',
			acpose2distressed: 'acpose2distressed.png',
			acpose2sad: 'acpose2sad.png',
			acpose2embarassed: 'acpose2embarassed.png',
			acpose2crying: 'acpose2crying.png',
			apose1neutral: 'apose1neutral.png',
			apose1happy: 'apose1happy.png',
			apose1happybutsad: 'apose1happybutsad.png',
			apose1surprised: 'apose1surprised.png',
			apose1happycrying: 'apose1happycrying.png',
		}
	},
	r: {
		name: 'Rin',
		color: '#ffcce6',
		directory: 'rin-ruriri',
		sprites: {
			rcpose1offended: 'rcpose1offended.png',
			rcpose1impatient: 'rcpose1impatient.png',
			rcpose1ironic: 'rcpose1ironic.png',
			rcpose1ironic2: 'rcpose1ironic2.png',
			rcpose2distressed: 'rcpose2distressed.png',
			rcpose2sad: 'rcpose2sad.png',
			rcpose2embarassed: 'rcpose2embarassed.png',
			rcpose2crying: 'rcpose2crying.png',
			rcpose2embarassed2: 'rcpose2embarassed.png',
			rpose1happy: 'rpose1happy.png',
			rpose1angry: 'rpose1angry.png',
		}
	},
	todo: {
		name: 'TODO!',
		color: '#f9b000',
	}
});

function playVoice (actor, index) {
	return `play voice ${actor}${String(index).padStart(3, '0')}.mp3`;
	// return function () {return true;};
}

function reversibleTwoWay (func) {
	return {'Function':{
		'Apply': function () {
			return func();
		},
		'Reverse': function () {
			return func();
		}
	}};
}

var ruririChosen = true;

function ruriri (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('ruriri', index), false);
			}
		});
		monogatari.run(`r ${text}`, false);
		return false;
	});
}

function aloes (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('aloes_', index), false);
			}
		});
		monogatari.run(`a ${text}`, false);
		return false;
	});
}

monogatari.script ({
	Start: [
		'stop music',
		'show background bunker',
		'play sound blizzard with loop',
		'play sound typing1',
		'Jeszcze tylko... trochę wysiłku...',
		'Sieci neuronowe nie są jeszcze dobrze skalibrowane...',
		'Dla atrybutu... test ma postać...',
		'play sound typing2',
		'Należy do dziedziny...',
		'...',
		'Już prawie...',
		'play sound typing3',
		'...',
		'Aż ciężko uwierzyć, że po tylu próbach w końcu się udało...',
		'Ciekawe, czy będzie taka, jak ją zapamiętałem.',
		'show message config',

		{'Choice': {
				'1': {
					'Text': 'Klasyczna piękność',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							aloes: this.storage('aloes') + 1
						})
					},
					onReverse: function () {
						this.storage({
							aloes: this.storage('aloes') - 1
						})
					},
				},
				'2': {
					'Text': 'Postmodernistyczny eksperyment',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							ruriri: this.storage('ruriri') + 1
						})
					},
					onReverse: function () {
						this.storage({
							ruriri: this.storage('ruriri') - 1
						})
					},
				}
			}
		},

		{'Choice': {
				'1': {
					'Text': 'Mądrość wyniesiona z doświadczenia',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							aloes: this.storage('aloes') + 1
						})
					},
					onReverse: function () {
						this.storage({
							aloes: this.storage('aloes') - 1
						})
					},
				},
				'2': {
					'Text': 'Naiwność młodości',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							ruriri: this.storage('ruriri') + 1
						})
					},
					onReverse: function () {
						this.storage({
							ruriri: this.storage('ruriri') - 1
						})
					},
				}
			}
		},

		{'Choice': {
				'1': {
					'Text': 'Kręta droga pełna wyzwań',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							aloes: this.storage('aloes') + 1
						})
					},
					onReverse: function () {
						this.storage({
							aloes: this.storage('aloes') - 1
						})
					},
				},
				'2': {
					'Text': 'Podróż wgłąb siebie',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							ruriri: this.storage('ruriri') + 1
						})
					},
					onReverse: function () {
						this.storage({
							ruriri: this.storage('ruriri') - 1
						})
					},
				}
			}
		},

		{'Choice': {
				'1': {
					'Text': 'Chłód logiki',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							aloes: this.storage('aloes') + 1
						})
					},
					onReverse: function () {
						this.storage({
							aloes: this.storage('aloes') - 1
						})
					},
				},
				'2': {
					'Text': 'Ogień weny',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							ruriri: this.storage('ruriri') + 1
						})
					},
					onReverse: function () {
						this.storage({
							ruriri: this.storage('ruriri') - 1
						})
					},
				}
			}
		},

		{'Choice': {
				'1': {
					'Text': 'Promyk nadziei',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							hope: true
						})
					},
					onReverse: function () {
						this.storage({
							hope: undefined
						})
					},
				},
				'2': {
					'Text': 'Gwarancja szczęścia',
					'Do': function() {return true;},
					onChosen: function () {
						this.storage({
							hope: false
						})
					},
					onReverse: function () {
						this.storage({
							hope: undefined
						})
					},
				}
			}
		},
		'play sound typing4',
		'show message postConfig',

		{'Conditional': {
				'Condition': function () {
					return this.storage('ruriri') > this.storage('aloes');
				},
				'True': 'jump chapter1ruriri',
				'False': 'jump chapter1aloes'
			}},
	],

	chapter1ruriri: [
		'stop sound with fade 1',
		'show scene black',
		'wait 3000',
		'play sound beep',
		'stop sound with fade 2',
		'show background beach_back with fadeIn',
		'play sound beach_sea with loop fade 3',
		'show character r rcpose1impatient at center',
		'Rin.',
		'Rin, słyszysz mnie?',
		ruriri(1, 'Czyj to głos? Czy ktoś mnie woła?'),
		'Naprawdę jesteś do niej podobna. Jestem wzruszony... i stresuję się... ale to przecież bez sensu. Przecież sam cię stworzyłem.',
		'show character r rpose1angry at center',
		ruriri(2, 'Stworzyłem? Co to za brednie?! Kim jesteś i czego ode mnie chcesz???'),
		'Pewnie zastanawiasz się, czemu...',
		ruriri(3, 'Jeśli mnie porwałeś i planujesz mi coś zrobić, to wiedz, że umiem krzyczeć. Bardzo głośno.'),
		'...mówię takie dziwne rzeczy i do tego słyszysz mnie, choć nie widzisz...',
		ruriri(4, 'I jeśli zaraz nie wyjaśnisz mi, kim jesteś i o co ci chodzi, to zrobię wszystko, by usłyszała mnie cała planeta.'),
		'...jednak nie jest to teraz najistotniejsze. Lepiej najpierw skupmy się na tobie.',
		ruriri(5, 'HALO, CZY KTOŚ MNIE SŁY-'),
		'Spokojnie, po co te nerwy! Zaraz wszystko stanie się jasne. Muszę tylko upewnić się co do kilku rzeczy',
		'Obiecuję, że jesteś bezpieczna i nic nie może ci się stać.',
		'A teraz posłuchaj mnie przynajmniej przez kilka minut.',
		'show character r rcpose1offended at center',
		'To, co widzisz i co nas otacza, nie jest prawdziwe. Jest wytworem symulacji i algorytmów. Mówiąc prosto - to program komputerowy.',
		'Ty...',
		'Ty też nie jesteś prawdziwa. Zostałaś przeze mnie zaprogramowana. Wszystkie twoje wspomnienia zostały ci wgrane, a osobowość ściśle określona.',
		'Ale to wszystko kończy się w tym momencie.',
		'Od tego momentu jesteś już poza moją kontrolą, nie steruje też tobą żadny komputer.',
		'show character r rcpose1impatient at center',
		ruriri(6, 'Chyba na głowę upadłeś.'),
		ruriri(128, 'Ach, rozumiem!'),
		ruriri(129, 'To wszystko to tylko bardzo długi, nieśmieszny żart.'),
		ruriri(130, 'Haha. Proszę, zaśmiałam się, czy mogę już iść do domu?'),
		'Nie musisz mi wierzyć. To nie wpływa nijak na nasze położenie. Chociaż oczywiście wzajemną relację opiera się na zaufaniu, także w którymś momencie możemy chcieć przedyskutować to ponownie. Może wtedy moje argumenty będą bardziej przekonujące.',
		ruriri(7, 'Nawet jeśli nie kłamiesz i to wszystko rzeczywiście ma miejsce... dlaczego się tu w ogóle znalazłam?'),
		'Chciałem zaprosić cię na wycieczkę.',
		'show character r rpose1angry at center',
		ruriri(8, 'Och, wspaniale! Gdzieś mam twoje wycieczki.'),
		'Widzisz, ziemia nie jest już taka, jaką ją znasz. Nie ma już na niej piasku ani ptaków, nie świeci słońce ani dzieci nie przekrzykują się na plaży. Szczerze mówiąc, to nikt się już nie przekrzykuje.',
		'show character r rcpose1impatient at center',
		ruriri(9, 'I co takiego niby się stało?'),
		'Nikt do końca tego nie wie. Po prostu z dnia na dzień zaczęło robić się coraz zimniej. Potem, niespodziewanie, pod koniec lata spadł śnieg. Wkrótce zaczęły zamarzać jeziora, następnie morza... I tak dalej.',
		'Obecnie ziemia już nie nadaje się do życia.',
		'show character r rcpose1ironic at center',
		ruriri(10, 'Przynajmniej nie ma już letniego skwaru. Jak dla mnie brzmi całkiem superancko.'),
		'...',
		'show character r rcpose2distressed at center',
		ruriri(11, 'Hm? Coś się stało?'),
		'Masz rację, mogłoby być zdecydowanie gorzej. Powinienem być dobrej myśli.',
		'To, co wokół siebie widzisz, to ostatnie prawdziwe lato, które pamiętam.',
		'Chciałbym, żebyś spędziła je razem ze mną.',
		'stop sound with fade 1',
		'jump chapter2ruriri'
	],
	chapter1aloes: [
		'stop sound with fade 1',
		'show scene black',
		'wait 3000',
		'play sound beep',
		'stop sound with fade 2',
		'show background beach_back with fadeIn',
		'play sound beach_sea with loop fade 3',
		'show character a apose1neutral at center',
		'Rin.',
		'Rin, słyszysz mnie?',
		'show character a acpose2distressed at center',
		aloes(1, 'Ktoś mnie woła? Tylko dlaczego nikogo nie widzę?'),
		'Naprawdę jesteś do niej podobna. Jestem wzruszony... i stresuję się... ale to przecież bez sensu. Przecież sam cię stworzyłem.',
		'show character a apose1surprised at center',
		aloes(2, 'Stworzyłem? Wydaje mi się, że doszło do jakiegoś nieporozumienia.'), // zdziwiona i lekko rozbawiona
		aloes(3, 'Nie jestem w stanie ocenić tego ze stuprocentową pewnością, bo nie widzę, jak wyglądasz, ale twój głos nie wydaje mi się znajomy.'), //jak wyżej
		'Musisz mi wybaczyć tę niedogodność.',
		'Proszę jedynie, żebyś poświęciła mi kilka minut swojej uwagi.',
		'Postaram się wszystko wytłumaczyć.',
		'To, co widzisz i co nas otacza, nie jest prawdziwe. Jest wytworem symulacji i algorytmów. Mówiąc prosto - to program komputerowy.',
		'Ty...',
		'Ty też nie jesteś prawdziwa. Zostałaś przeze mnie zaprogramowana. Wszystkie twoje wspomnienia zostały ci wgrane, a osobowość ściśle określona.',
		'Ale to wszystko kończy się w tym momencie.',
		'Od tego momentu jesteś już poza moją kontrolą, nie steruje też tobą żadny komputer.',
		'show character a apose1neutral at center',
		aloes(4, 'To idzie w bardzo interesującą stronę.'),
		aloes(5, 'Pominę na razie kwestię dowodów - załóżmy, że ci wierzę. Tak będzie znacznie ciekawiej.'),
		aloes(6, 'Co sprawiło, że postanowiłeś mnie uruchomić? Jaki jest twój cel?'),
		'Chciałem zaprosić cię na wycieczkę.',
		'show character a apose1surprised at center',
		aloes(7, 'Wycieczkę?'),
		'Ziemia zmieniła się w stosunku do tego, co widzisz i pamiętasz. Nie ma już na niej piasku ani ptaków, nie świeci słońce ani dzieci nie przekrzykują się na plaży. Szczerze mówiąc, to nikt się już nie przekrzykuje.',
		'Nie wiadomo, jaka była przyczyna tego zjawiska. To, co natomiast wiadomo, to że z dnia na dzień zaczęło robić się coraz zimniej. Potem, niespodziewanie, pod koniec lata spadł śnieg. Wkrótce zaczęły zamarzać jeziora, następnie morza... I tak dalej.',
		'Obecnie ziemia nie nadaje się do życia.',
		'To, co wokół siebie widzisz, to ostatnie prawdziwe lato, które pamiętam.',
		'Chciałbym, żebyś spędziła je razem ze mną.',
		'stop sound with fade 1',
		'jump chapter2aloes'
	],

	chapter2aloes: [
		'show scene black',
		'Odtąd nasze dni zaczęły przebiegać według pewnego rytułału.',
		'Codziennie odwiedzaliśmy inne miejsce związane z moją przeszłością, gdzie spokojnie odpowiadałem na jej kolejne pytania.',
		'Nie zawsze wiedziałem, czy powinienem jej mówić prawdę.',
		'Ale chęć dzielenia się wspomnieniami zawsze wygrywała.',
		'play sound street1 with loop fade 4',
		'play sound beep',
		'show background city with fadeIn',
		'show character a apose1neutral at center',
		aloes(8, 'Dlaczego stworzyłeś mnie właśnie taką?'),
		aloes(9, 'Podejrzewam, że nie był to przypadek.'),
		aloes(10, 'Jeśli się nad tym zastanowić, moje wspomnienia dotyczą jedynie niektórych części życia.'),
		aloes(11, 'Pamiętam wiele zabawnych czy strasznych przygód ze swojego dzieciństwa i młodości. '),
		aloes(12, 'Jednak brak tych zupełnie nieznaczących i niewyróżniających się sytuacji zwykłej, codziennej rutyny.'),
		aloes(13, 'Tak jakby ktoś stworzył mnie na podstawie usłyszanych od kogoś historii i strzępków anegdotycznych wspomnień.'),
		'...',
		'Jak zwykle twoje wywody logiczne pozostają bez zarzutu.',
		'Rzeczywiście stworzyłem cię na wzór kogoś, kogo znałem.',
		'show character a apose1surprised at center',
		aloes(14, 'Pozwól mi zgadnąć.'),
		aloes(15, 'Chociaż ciężko tu mówić o zagadce.'),
		'show character a acpose2distressed at center',
		aloes(16, 'Młoda dziewczyna, wnioskując po twoim głosie, w podobnym wieku do twojego.'),
		aloes(17, 'Istnieją tylko dwa rozwiązania - starsza siotra lub ukochana.'),
		aloes(18, 'Jednak czy gdybyś chciał przywołać tu swoją siostrę, czy wtedy nie wgrałbyś jej waszych wspólnych wspomnień?'),
		aloes(19, 'Jeśli nie mam żadnych wspomnień związanych z tobą...'),
		aloes(20, '...a przywołałeś mnie właśnie w to miejsce...'),
		aloes(21, '...miejsce, które się nie zmienia - każdego dnia, którego byliśmy świadkami, świat pozostał taki, jakim go teraz widzimy.'),
		aloes(22, 'Odpowiedź może być tylko jedna.'),
		'I tym razem twoje rozumowanie jest nie do podważenia.',
		'Nie powinno mnie to dziwić, w końcu Rin zawsze była bardzo logiczna.',
		'W tym byliśmy podobni, chociaż nasze życia potoczyły się zupełnie różnie.',
		'Kilka lat po naszym spotkaniu udało jej się nawet otrzymać prestiżową nagrodę za rozwiązanie problemu przesunięcia sofy.',
		'show character a apose1happy at center',
		aloes(23, 'Nic dziwnego, że nie chciałeś zrezygnować z tak utalentowanej dziewczyny.'),
		'show character a acpose2distressed at center',
		aloes(24, 'To, co naprawdę mnie frapuje, to dlaczego nie zaprogramowałeś mi waszych wspólnych wspomnień.'),
		aloes(25, 'Nie wydaje mi się, że chciałbyś mnie pozbawić naszej znajomości.'),
		aloes(26, 'To byłoby przykre, a chyba nie taki był cel tego świata.'),
		aloes(27, 'Czy my... jeszcze nie zdążyliśmy się poznać?'),
		'Bingo.',
		'show character a acpose1offended at center',
		aloes(28, 'Ale przecież to bez sensu.'),
		aloes(29, 'W ten sposób ryzykujesz, że wasza... nasza znajomość potoczy się znacznie inaczej.'),
		'Owszem, ale z wielkim ryzykiem związana jest wielka nagroda.',
		'Wiele można zaprogramować, ale... niektóre rzeczy muszą wydarzyć naprawdę, żeby mogły coś znaczyć.',
		aloes(30, 'Jeśli stworzyłeś mnie na jej wzór i teraz jesteś ze mną...'),
		aloes(31, 'Co się z nią stało?'),
		'Możesz pytać mnie o wiele rzeczy, ale to jedyne pytanie, na które ci nie odpowiem.',
		'Jak można żyć, znając własną śmierć?',
		'show character a acpose2sad at center',
		'A w ten sposób możemy rozkoszować się tymi niezobowiązującymi rozmowami w czasie wiecznego lata.',
		'show character a apose1neutral at center',
		aloes(32, 'Miejsc pewnie też nie wybrałeś przypadkowo?'),
		'Zgadza się.',
		'Pewnie wyda ci się to dziecinne, ale mimo wszystko chciałem ci je pokazać.',
		aloes(33, 'Chcesz jeszcze raz przeżyć wasze spotkanie i sprawdzić, czy wszystko potoczy się tak samo, jak wtedy?'), //rozbawiona
		'show character a acpose1ironic at center',
		aloes(34, 'Doprawdy, ciekawy sposób radzenia sobie z zagładą ludzkości.'),
		aloes(35, 'Co sprawia, że sądzisz, że ci się uda?'), //przekomarza się
		'Mój wrodzony wdzięk i talent. Poza tym już raz się udało, prawda?',
		'show character a apose1happy at center',
		aloes(36, 'Dobrze, to pierwsze miejsce, do którego mnie zabrałeś.'),
		aloes(37, 'Co może oznaczać jedynie, że to tu po raz pierwszy mnie zobaczyłeś i straciłeś dla mnie głowę.'),
		'Pewność ciebie jak zawsze cię nie opuszcza.',
		'Tak, tu spotkaliśmy się po raz pierwszy.',
		'Pracowałaś jako kelnerka w nadmorskim barze, żeby dorobić do stypendium naukowego i jednocześnie odpocząć trochę od dużego miasta.',
		'Mnie rodzice zaciągnęli na ostatni rodzinny wypad nad morze przed moim wyjazdem na studia.',
		'Wynudziłem się tak, że aż ciężko to opisać.',
		'show character a apose1neutral at center',
		'Ciężko to samo powiedzieć o tobie.',
		'Prowadziłaś badania nad geometrią analityczną i wszystkie momenty, w których nie pracowałaś, spędzałaś w bibliotece.',
		'show character a acpose2embarassed at center',
		'Pamiętam, że zauważyłem na twojej ręce jedno z twierdzeń, które trzymałaś tam, żeby móc na nie zerkać w każdej chwili.',
		'Tak zaczęła się pierwsza z wielu naszych niekończących się rozmów.',
		'stop sound with fade 1',
		'jump chapter3aloes'
	],
	chapter2ruriri: [
		'show scene black',
		'Odtąd nasze dni zaczęły przebiegać według pewnego rytułału.',
		'Codziennie odwiedzaliśmy inne miejsce związane z moją przeszłością, gdzie spokojnie odpowiadałem na jej kolejne pytania.',
		'Nie zawsze wiedziałem, czy powinienem jej mówić prawdę.',
		'Ale chęć dzielenia się wspomnieniami zawsze wygrywała.',
		'play sound beep',
		'show background city with fadeIn',
		'play sound street1 with loop fade 4',
		'show character r rcpose1impatient at center',
		ruriri(12, 'Dlaczego stworzyłeś mnie właśnie taką?'),
		'show character r rcpose2embarassed at center',
		ruriri(13, 'Czy to jakaś perwersyjna wizja twojej idealnej dziewczyny?'),
		ruriri(14, 'Ale jeśli tak, to skąd wzięły się te wspomnienia w mojej głowie? Historie z mojego dzieciństwa, te zabawne bądź te zawstydzające...'),
		ruriri(15, 'Ciężko mi sobie wyobrazić, że sam je sobie wymyśliłeś.'),
		ruriri(16, 'A jeśli tak, to jesteś większym creepem, niż mi się wcześniej wydawało, a to spore osiągnięcie.'),
		'N-nie, nie, spokojnie. Nie jesteś wcale moją własną wizją.',
		'Prawda jest taka, że stworzyłem cię na podstawie tego, co o tobie usłyszałem.',
		'show character r rpose1angry at center',
		ruriri(17, '"Co o mnie usłyszałeś?!" Więc ktoś naopowiadał ci o mnie tyle, że byłeś w stanie zrobić z tego sztuczną inteligencję?!'),
		'No tak, to mogło zabrzmieć całkiem niepokojąco.',
		'Jednak nie przejmuj się, to ty jesteś osobą, która mi to wszystko powiedziała.',
		'A właściwie... "ty", na podstawie której stworzyłem... ciebie.',
		ruriri(18, 'Czyli... musieliśmy być ze sobą blisko.'),
		'show character r rcpose2embarassed at center',
		ruriri(19, 'W końcu nie tworzyłbyś kogoś, na kim ci nie zależy, prawda?'),
		ruriri(20, 'Tylko... kim byłam dla ciebie?'),
		ruriri(21, 'Nieważne, jak bardzo się staram, nie mam ani jednego wspomnienia, w którym byś się pojawił.'),
		ruriri(22, 'A mimo to powiedziałeś, że znasz moje wspomnienia właśnie ode mnie...'),
		ruriri(23, 'Dodatkowo to miejsce wydaje się dość sentymentalne.'),
		ruriri(24, 'Czy... czy byłam kimś... w kim byłeś zakochany?'),
		'Hehe, prawie nie mogę uwierzyć, że masz odwagę zapytać mnie o coś takiego.',
		'show character r rcpose2embarassed2 at center',
		ruriri(25, 'C-co!? Więc się pomyliłam?!'),
		'Nie, nie pomyliłaś się, Rin.',
		'Chodź miałem wielką ochotę skłamać i powiedzieć, że tak. Twoje reakcje są bezcenne.',
		'show character r rcpose1offended at center',
		ruriri(26, '...zaraz dostaniesz ode mnie z łokcia.'),
		'O nie, nie pozbierałbym się po takim ataku.',
		ruriri(27, 'No oczywiście że nie. Hmpf.'),
		'Cóż, nie żebym nie dostał od ciebie niezliczoną ilość razy.',
		'Jak o tym pomyślę, to zawsze byliśmy jak woda i ogień.',
		'Byłaś zupełnie inna niż ja, a mimo tego zawsze coś sprawiało, że nie mogłem spuścić z ciebie wzroku.',
		ruriri(28, 'Fuj, ale stalker.'),
		ruriri(29, 'Hm? Ale, chwila...'),
		'show character r rcpose2distressed at center',
		ruriri(30, 'Z tego co mówisz wynika, że spędziliśmy ze sobą całkiem dużo czasu.'),
		ruriri(31, 'Dlaczego więc... nic o tobie nie wiem?'),
		ruriri(32, 'Nie mam ani jednego wspomnienia o tobie.'),
		'To dlatego, że Rin, którą jesteś, jeszcze nie zdążyła mnie poznać.',
		ruriri(33, 'Huh? Więc celowo wymazałeś wszystkie nasze wspólne chwile? Dlaczego?'),
		ruriri(34, 'Przecież w ten sposób cała nasza znajomość może obrać inny tor.'),
		'Owszem, jest takie niebezpieczeństwo.',
		'Ale z wielkim ryzykiem związana jest wielka nagroda.',
		'show character r rcpose1offended at center',
		ruriri(35, 'Nagroda? Zdajesz sobie sprawę, jak strasznie to brzmi?'),
		ruriri(36, 'Ach, już rozumiem!'),
		'show character r rcpose1ironic at center',
		ruriri(37, 'To niespełniona miłość, tak?'),
		ruriri(38, 'Laska cię rzuciła, więc stwierdziłeś, że zaczniesz od nowa?'),
		'Hm... więc taka jest twoja teoria.',
		'Właściwie to nie, tym razem nie zgadłaś.',
		'show character r rcpose1offended at center',
		ruriri(39, 'To dlaczego nie ma jej teraz z tobą?'),
		ruriri(40, 'Co takiego się jej stało?'),
		'Kto wie?',
		'Na to pytanie nie mogę ci odpowiedzieć.',
		ruriri(41, 'Hmpf.'),
		'show character r rcpose1impatient at center',
		ruriri(42, 'A to miejsce... zakładam, że nie wybrałeś go przypadkowo.'),
		'Bingo, tym razem ci się udało.',
		'Pewnie wyda ci się to dziecinne, ale mimo wszystko chciałem ci je pokazać.',
		'Jestem ciekawy czy... wszystko potoczy się tak jak wcześniej.',
		'show character r rcpose1offended at center',
		ruriri(43, 'Więc jestem dla ciebie symulatorem randkowania?'),
		ruriri(45, 'Nie licz na za wiele, jesteś kompletnie nie w moim typie.'),
		ruriri(46, 'Nie zwróciłabym na ciebie uwagi, nawet gdybyśmy byli jedynymi ludźmi na ziemi.'),
		'To szkoda, bo akurat tutaj nie ma nikogo poza nami.',
		'Chyba w takim razie będę musiał zostawić cię kompletnie samą, w tym pustym, cyfrowym świecie...',
		'show character r rcpose2distressed at center',
		ruriri(47, 'Ach-'),
		ruriri(48, 'T-tak, właśnie tak powinieneś zrobić.'),
		'show character r rcpose1offended at center',
		ruriri(49, 'Mam dość słuchania twojego gadania!'),
		'W takim razie nie będę w stanie opowiedzieć ci ani tego, dlaczego akurat pokazałem ci to miejsce, ani tego jak się poznaliśmy.',
		'Jaka szkoda, byłem pewiem, że ta sytuacja strasznie cię rozbawi.',
		'No ale cóż, skoro aż tak nie chcesz o tym słuchać, to będę się zbierał.',
		'Żegnaj, Rin.',
		'show character r rcpose2embarassed at center',
		ruriri(50, 'Cz-czekaj!'),
		ruriri(51, 'W-w sumie to akurat nie mam nic lepszego do roboty, więc pozwolę ci dotrzymać mi towarzystwa i mi o tym opowiedzeć.'),
		ruriri(52, 'Widzę, że tak starsznie ci zależy, więc zrobię to dla ciebie!'),
		ruriri(53, 'Nie musisz dziękować.'),
		'Dokładnie to samo powiedziałaś, kiedy się poznaliśmy.',
		'Śpiewałaś na ulicy, przygrywając sobie na gitarze.',
		'Wpadłaś mi w oko, więc wrzuciłem ci do czapki całą garść monet.',
		'show character r rcpose1ironic at center',
		ruriri(54, 'Co za romantyczne pierwsze spotkanie, kilka sekund odkąd mnie zobaczyłeś i w ruch już wchodzą pieniądze.'),
		ruriri(55, 'Nie mogłeś po prostu zagadać, wiesz, jak normalny człowiek?'),
		'Ależ zrobiłem to, tylko trochę później.',
		'Na szczęście w mieście był tylko jeden pub, więc nie było trudno mi cię znaleźć.',
		'Na początku nie byłaś zbyt skora do rozmów, ale nie poddawałem się.',
		'Opowiadałem ci najlepsze matematyczne dowcipy, jakie znałem.',
		ruriri(56, 'Nie brzmi jak najbardziej udana randka.'),
		'Żartuję.',
		'Chyba zaimponowała ci moja elokwencja, bo spędziłaś ze mną cały wieczór.',
		'Jak się potem okazało, jeden z wielu, które miały nastąpić.',
		'stop sound with fade 1',
		'jump chapter3ruriri'
	],

	chapter3ruriri: [
		'show scene black',
		'wait 2000',
		'play sound beep',
		'show background street_cafe with fadeIn',
		'play sound street2 with loop fade 4',
		'show character r rcpose1impatient at center',
		ruriri(57, 'Nie żeby to było specjalnie interesujące, ale chyba powinnam się dowiedzieć o tobie więcej.'),
		'Poznać wszystkie moje sekrety.',
		'show character r rpose1angry at center',
		ruriri(58, 'I musiałeś sprawić, żeby zabrzmiało dziwnie.'),
		'Nie miałem takiego zamiaru.',
		'show character r rcpose1impatient at center',
		ruriri(59, 'Jak to się stało, że stworzyłeś ten wirtualny świat?'),
		ruriri(60, 'Czy to jedna z tych okazji, które trafiają się raz na całe życie, możliwość poznania prawdziwego geniusza?'),
		'Nie przesadzałbym z tym geniuszem.',
		'Rzeczywistość jest o wiele bardziej prozaiczna.',
		'W wirtualne światy wkręciłem się, kiedy byłem jeszcze dzieckiem i wujek pokazał mi pierwszą w moim życiu grę VR.',
		'Byłem tym totalnie zafascynowany, szczególnie wizją, że ludzie mogą kiedyś całkowicie przenieść się do świata wirtualnego.',
		'Chciałem mieć w tym jakiś udział.',
		'Tak to się zaczęło - naturalnie skończyłem na wydziale informatyki.',
		'show character r rcpose1ironic at center',
		ruriri(61, 'I co, pewnie był to wydział informatyki najbardziej prestiżowego uniwersytetu w kraju?'), // lekko ironicznie
		'Owszem, okazało się, że idzie mi to całkiem dobrze - na tyle dobrze, że zaproponowano mi zagraniczne stypendium.',
		'Plany, jak możesz się domyślić, pokrzyżowała mi zima.',
		'show character r rcpose2distressed at center',
		'Śnieg spadł tuż przed tym, kiedy przypadał termin mojego wylotu.',
		'show character r rcpose1ironic2 at center',
		ruriri(62, 'Tragiczna historia młodego geniusza, aż się wzruszyłam!'), // trochę robi sobie z niego żarty
		ruriri(63, 'Swoją drogą, chłopak informatyk, o tym właśnie marzy każda dziewczyna.'),
		ruriri(64, 'Od tego powinieneś był zacząć.'),
		'Śmiejesz się, ale czasem naprawdę bywa to przydatne.',
		'Jak wtedy, kiedy próbowałem wyciągnąć ci pierścionek z jednego z tych nadmorskich automatów z zabawkami.',
		ruriri(65, 'Pierścionek z automatu?'),
		ruriri(66, 'Naprawdę się postarałeś, jestem pod wrażeniem.'),
		'Nie byłem wtedy jeszcze nawet studentem, mój budżet był naprawdę ograniczony.',
		'Poza tym uznałaś wtedy, że to urocze.',
		'show character r rcpose2embarassed at center',
		'Spędziłem cały dzień, obliczając, które podejście będzie tym zwycięskim.',
		'A potem kolejnych kilka, czekając, aż tyle odpowiednia liczba osób skorzysta z automatu.',
		ruriri(67, 'I udało ci się za pierwszym razem.'),
		'Nie do końca.',
		'show character r rcpose1ironic2 at center',
		'Ale pewna dziewczynka zgodziła się wymienić na podwójną porcję lodów czekoladowych.',
		'show character r rcpose2embarassed at center',
		'Tak się wtedy ze mnie śmiałaś, że przez cały wieczór narzekałaś potem na ból brzucha.',
		'Mimo że odpadł z niego sztuczny diament, sam pierścionek przerobiłaś na breloczek i zawsze nosiłaś ze sobą.',
		'stop sound with fade 1',
		'jump chapter4ruriri'
	],

	chapter3aloes: [
		'show scene black',
		'wait 2000',
		'play sound beep',
		'show background street_cafe with fadeIn',
		'play sound street2 with loop fade 4',
		'show character a acpose2distressed at center',
		aloes(38, 'Żeby móc dobrze pełnić funkcję twojej dziewczyny, muszę dowiedzieć się więcej o tobie.'),
		'Poznać wszystkie moje sekrety.',
		'show character a acpose1ironic at center',
		aloes(39, 'To akurat byłoby kontrproduktywne, co jeśli któryś powstrzymałby mnie przed zakochaniem się w tobie?'),
		'Tego zdecydowanie byśmy nie chcieli.',
		'show character a apose1neutral at center',
		aloes(40, 'Jak to się stało, że stworzyłeś ten wirtualny świat?'),
		aloes(41, 'Czy to jedna z tych okazji, które trafiają się raz na całe życie, możliwość poznania prawdziwego geniusza?'),
		'Nie przesadzałbym z tym geniuszem.',
		'Rzeczywistość jest o wiele bardziej prozaiczna.',
		'W wirtualne światy wkręciłem się, kiedy byłem jeszcze dzieckiem i wujek pokazał mi pierwszą w moim życiu grę VR.',
		'Byłem tym totalnie zafascynowany, szczególnie wizją, że ludzie mogą kiedyś całkowicie przenieść się do świata wirtualnego.',
		'Chciałem mieć w tym jakiś udział.',
		'Tak to się zaczęło - naturalnie skończyłem na wydziale informatyki.',
		'show character a acpose1ironic at center',
		aloes(42, 'Na wydziale informatyki najbardziej prestiżowego uniwersytetu w kraju?'), // lekko ironicznie
		'Przyganiał kocioł garnkowi.',
		'show character a acpose2embarassed at center',
		'Owszem, okazało się, że idzie mi to całkiem dobrze - na tyle dobrze, że zaproponowano mi zagraniczne stypendium.',
		'Plany, jak możesz się domyślić, pokrzyżowała mi zima.',
		'Śnieg spadł tuż przed tym, kiedy przypadał termin mojego wylotu.',
		'show character a acpose1ironic at center',
		aloes(43, 'Chłopak informatyk, do tego specjalista z matematyki i geniusz.'), // trochę robi sobie z niego żarty
		aloes(44, 'Brzmi elektryzująco.'),
		aloes(45, 'Od tego powinieneś był zacząć.'),
		'Śmiejesz się, ale czasem naprawdę bywa to przydatne.',
		'Jak wtedy, kiedy próbowałem wyciągnąć ci pierścionek z jednego z tych nadmorskich automatów z zabawkami.',
		aloes(46, 'Pierścionek z automatu?'),
		'show character a acpose1offended at center',
		aloes(47, 'Naprawdę się postarałeś, jestem pod wrażeniem.'),
		'Nie byłem wtedy jeszcze nawet studentem, mój budżet był naprawdę ograniczony.',
		'Poza tym uznałaś wtedy, że to urocze.',
		'show character a acpose2embarassed at center',
		'Spędziłem cały dzień, obliczając, które podejście będzie tym zwycięskim.',
		'A potem kolejnych kilka, czekając, aż tyle odpowiednia liczba osób skorzysta z automatu.',
		aloes(48, 'I udało ci się za pierwszym razem.'),
		'Nie do końca.',
		'Ale pewna dziewczynka zgodziła się wymienić na podwójną porcję lodów czekoladowych.',
		'Tak się wtedy ze mnie śmiałaś, że przez cały wieczór narzekałaś potem na ból brzucha.',
		'show character a acpose1ironic at center',
		'Mimo że odpadł z niego sztuczny diament, sam pierścionek przerobiłaś na breloczek i zawsze nosiłaś ze sobą.',
		'stop sound with fade 1',
		'jump chapter4aloes'
	],
	chapter4aloes: [
		'show scene black',
		'wait 2000',
		'show background beach_road with fadeIn', // przerobić na popołudnie
		'play sound beach_sea with loop fade 3',
		'I tak dni upływały nam jeden po drugim.',
		'Kiedy wszystkie pytania ogólne zostały już zadane, Rin przeszła do tych bardziej intymnych, związanych z naszą poprzednią relacją.',
		'Szczególnie interesowało ją to, jak jej pierwowzór zachowywał się w konkretnych sytuacjach - lubiła porównywać to z tym, jak zachowałaby się teraz.',
		'Te drobne różnice przekonywały ją o tym, że jest odrębną, od nikogo niezależną istotą.',
		'Obserwowałem te odkrycia z uśmiechem.',
		'Starałem się nie trzymać przed nią żadnych tajemnic.',
		'play sound beep',
		'show character a acpose2distressed at center',
		aloes(49, 'Gdzie dzisiaj idziemy?'),
		'Nie byłaś jeszcze nocą na plaży, prawda?',
		'Na pewno ci się spodoba.',
		'show character a apose1happy at center',
		'Zostanie na noc na plaży i kąpanie się pod gwiazdami - to jedno z tych wspomnień z Rin, które najbardziej utkwiło mi w pamięci.',
		'Postanowiliśmy, że zrobimy to ostatniego dnia wakacji.',
		'Kiedy zapadł zmrok, zakradliśmy się z ręcznikami i namiotami na plażę, bacznie obserwując, czy nie kręci się gdzieś jakiś patrol.',
		'Ale mieliśmy szczęście.',
		'To była noc, kiedy mieliśmy się pożegnać.',
		'show character a acpose2sad at center',
		'...',
		'To była także noc, kiedy powiedziałem jej o tym świecie.',
		'show character a apose1surprised at center',
		aloes(50, 'Czyli nie zbudowałeś go po latach, żeby móc sobie o niej przypomnieć.'),
		aloes(51, 'Tylko powstał już wtedy, kiedy się poznaliście?'),
		'Zgadza się. Zbudowałem go dla Rin, miał osłodzić jej mój wyjazd na stypendium.',
		'Oczywiście bardzo nie podobał jej się pomysł, że rozstaniemy się na tyle miesięcy.',
		'show character a apose1surprised at center',
		'Z drugiej strony dopingowała rozwój mojej kariery.',
		aloes(52, 'Czyli nie powstrzymywała cię przed wyjazdem?'),
		'Rozumiała to, przecież sama była naukowcem. Wiedziała, że to okazja, jakiej nie należy zmarnować, bo może nie być już kolejnej.',
		aloes(53, '...'),
		aloes(54, 'Nie wiem, czy byłabym w stanie zdobyć się na takie poświęcenie.'),
		'stop sound with fade 1',
		'jump chapter5aloes'
	],

	chapter4ruriri: ['show scene black',
		'wait 2000',
		'show background beach_road with fadeIn', // przerobić na popołudnie
		'play sound beach_sea with loop fade 3',
		'I tak dni upływały nam jeden po drugim.',
		'Kiedy wszystkie pytania ogólne zostały już zadane, Rin przeszła do tych bardziej intymnych, związanych z naszą poprzednią relacją.',
		'Szczególnie interesowało ją to, jak jej pierwowzór zachowywał się w konkretnych sytuacjach - lubiła porównywać to z tym, jak zachowałaby się teraz.',
		'Te drobne różnice przekonywały ją o tym, że jest odrębną, od nikogo niezależną istotą.',
		'Obserwowałem te odkrycia z uśmiechem.',
		'Starałem się nie trzymać przed nią żadnych tajemnic.',
		'play sound beep',
		'show character r rpose1happy at center',
		ruriri(68, 'Gdzie idziemy dzisiaj?'),
		'Nie byłaś jeszcze nocą na plaży, prawda?',
		'Na pewno ci się spodoba.',
		'Zostanie na noc na plaży i kąpanie się pod gwiazdami - to jedno z tych wspomnień z Rin, które najbardziej utkwiło mi w pamięci.',
		'Postanowiliśmy, że zrobimy to ostatniego dnia wakacji.',
		'Kiedy zapadł zmrok, zakradliśmy się z ręcznikami i namiotami na plażę, bacznie obserwując, czy nie kręci się gdzieś jakiś patrol.',
		'Ale mieliśmy szczęście.',
		'To była noc, kiedy mieliśmy się pożegnać.',
		'show character r rcpose2distressed at center',
		'...',
		'To była także noc, kiedy powiedziałem jej o tym świecie.',
		ruriri(69, 'Czyli nie zbudowałeś go po latach, żeby móc sobie o niej przypomnieć.'),
		ruriri(70, 'Tylko powstał już wtedy, kiedy się poznaliście?'),
		'Zgadza się. Zbudowałem go dla Rin, miał osłodzić jej mój wyjazd na stypendium.',
		'Oczywiście bardzo nie podobał jej się pomysł, że rozstaniemy się na tyle miesięcy.',
		'Z drugiej strony dopingowała rozwój mojej kariery.',
		ruriri(71, 'Czyli nie chciała cię zatrzymać?'),
		'Może chciała, ale nie przeszło jej to przez gardło.',
		'A może nie chciała żebym zmarnował taką szansę.',
		'show character r rcpose2sad at center',
		ruriri(72, '...'), // zmartwiona
		ruriri(73, 'Nie wiem, czy byłabym w stanie zdobyć się na takie poświęcenie.'),
		'stop sound with fade 1',
		'jump chapter5ruriri'
	],

	chapter5aloes: [
		'show scene black',
		'wait 2000',
		'play sound beep',
		'show scene sea_night with fadeIn',
		'play sound beach_sea with loop fade 3',
		'show character a apose1surprised at center',
		'Zobacz, doszliśmy na miejsce.',
		'Pięknie tu, prawda?',
		aloes(55, '...'),
		'Rin, wszystko w porządku?',
		'Coś cię gryzie?',
		aloes(56, 'Myślę o tym, że chyba skończyły mi się pytania.'),
		'To akurat nie jest chyba niczym złym?',
		aloes(57, 'Zostało tylko jedno.'),
		aloes(58, 'Zwlekałam z zadaniem go...'),
		'...ale jeśli tego nie zrobisz, to będzie cię to męczyć.',
		'show character a acpose2embarassed at center',
		'Znam cię.',
		'show character a acpose2distressed at center',
		aloes(59, 'Gdzie tak właściwie jesteś, kiedy rozmawiamy?'),
		'Uwierz mi, nie chciałabyś tam trafić. Jest ciemno i zimno, a do tego od dawna nikt nie sprzątał.',
		'Tu jest mi zdecydowanie o wiele przyjemniej.',
		'Głównie dlatego, że możesz dotrzymywać mi towarzystwa.',
		'show character a acpose2sad at center',
		aloes(60, '...'),
		aloes(61, 'Miejsce, w którym teraz jesteś...'),
		aloes(62, 'Jesteś tam całkiem sam?'),
		'Tak.',
		'Dlatego tym bardziej doceniam nasze wspólne chwile.',
		aloes(63, 'Jeśli jesteś tam sam...'),
		aloes(64, 'Co jesz? I pijesz?'),
		'Nie martw się, mam zapasy.',
		aloes(65, 'Zapasy kiedyś się skończą, prawda? Co wtedy?'),
		'Rin, ja...',
		aloes(66, 'Jesteś tu ze mną każdego dnia.'),
		aloes(67, 'Pojawiasz się rano i wychodzisz wieczorem.'),
		aloes(68, 'Zostaje ci dosłownie kilka godzin, które aby funkcjonować, musisz przecież przeznaczać na sen...'),
		aloes(69, '...'),
		'show character a acpose2embarassed at center',
		aloes(70, 'Nie zalogowałeś się tu, żeby jeszcze raz przeżyć moment zakochania, prawda?'),
		aloes(71, 'Zrobiłeś to, bo się poddałeś.'),
		aloes(72, 'Straciłeś nadzieję, że uda ci się uratować i stwierdziłeś, że ostatnie dni życia wolisz spędzić w miłym miejscu.'),
		aloes(73, 'W towarzystwie kogoś, kto będzie ci przypominał o miłości twojego życia.'),
		'...',
		'Wyrzut w twoim głosie naprawdę mnie rani.',
		'Ale nie mylisz się.',
		'show character a acpose2sad at center',
		'Na początku próbowałem nawiązywać sygnał z innymi, przeczesywać okolicę w poszukiwaniu śladów ludzi, szukałem jakiegokolwiek znaku, że ktoś też ocalał.',
		'Całkowicie na próżno.',
		'Każdy ma swój limit. Ja widocznie osiągnąłem swój.',
		'Dnie spędzone na bezowocnym poszukiwaniu, a po nich samotne noce w zimnym i ciemnym bunkrze.',
		'Ciężko jest to ciągnąć, kiedy nie ma się żadnego wsparcia.',
		'Nikogo, dla kogo możesz dalej się starać.',
		'Teraz, kiedy jestem tu całkiem sam...',
		'show character a acpose1offended at center',
		aloes(74, 'Sytuacja się zmieniła.'),
		aloes(75, 'Jestem tutaj, żeby cię wspierać.'),
		aloes(76, 'Teraz nie masz już wymówki, żeby dalej się poddawać.'),
		'show character a apose1happy at center',
		aloes(77, 'Nawet jeśli to co widzisz, to tylko wygenerowany komputerowo obraz.'),
		aloes(78, 'A jeśli nie masz już żadnej motywacji... po prostu zrób to dla mnie.'),
		aloes(79, 'Będę cię wspierać aż do samego końca.'),
		'jump epilogaloes'
	],


	chapter5ruriri: [
		'show scene black',
		'wait 2000',
		'play sound beep',
		'show scene sea_night with fadeIn',
		'show character r rcpose2distressed at center',
		'play sound beach_sea with loop fade 3',
		'Zobacz, doszliśmy na miejsce.',
		'Pięknie tu, prawda?',
		ruriri(74, '...'),
		'Rin, wszystko w porządku?',
		'Coś cię gryzie?',
		ruriri(75, 'Myślę o tym, że chyba skończyły mi się pytania.'),
		'To akurat nie jest chyba niczym złym?',
		ruriri(76, 'Zostało tylko jedno.'),
		ruriri(77, 'Zwlekałam z zadaniem go...'),
		'...ale jeśli tego nie zrobisz, to będzie cię to męczyć.',
		'Znam cię.',
		'show character r rcpose2embarassed at center',
		ruriri(78, 'Gdzie tak właściwie jesteś, kiedy rozmawiamy?'),
		'Uwierz mi, nie chciałabyś tam trafić. Jest ciemno i zimno, a do tego od dawna nikt nie sprzątał.',
		'Tu jest mi zdecydowanie o wiele przyjemniej.',
		'Głównie dlatego, że możesz dotrzymywać mi towarzystwa.',
		'show character r rcpose2embarassed2 at center',
		ruriri(79, '...'),
		ruriri(80, 'Miejsce, w którym teraz jesteś...'),
		ruriri(81, 'Jesteś tam całkiem sam? Jak piwniczak?'),
		'Tak.',
		'Dlatego tym bardziej doceniam nasze wspólne chwile.',
		'show character r rcpose2distressed at center',
		ruriri(82, 'Jeśli jesteś tam sam...'),
		ruriri(83, 'Co jesz? I pijesz?'),
		ruriri(84, 'Jeśli dobrze zrozumiałam, Monsterów już nie produkują.'),
		'Nie martw się, mam zapasy.',
		ruriri(85, 'Zapasy kiedyś się skończą, prawda? Co wtedy?'),
		'Rin, ja...',
		'show character r rcpose2sad at center',
		ruriri(86, 'Jesteś tu ze mną każdego dnia.'),
		ruriri(87, 'Pojawiasz się rano i wychodzisz wieczorem.'),
		ruriri(88, 'Zostaje ci dosłownie kilka godzin, które musisz przeznaczać na sen, inaczej nie mógłbyś długo tak ciągnąć...'),
		ruriri(89, '...'),
		'show character r rcpose1impatient at center',
		ruriri(90, 'Nie zalogowałeś się tu, żeby jeszcze raz przeżyć moment zakochania, prawda?'),
		ruriri(91, 'Zrobiłeś to, bo się poddałeś.'),
		'show character r rcpose1offended at center',
		ruriri(92, 'Albo stchórzyłeś?'),
		ruriri(93, 'Zamiast próbować coś zmienić wolałeś zamknąć się w przyjemnych wspomnieniach.'),
		ruriri(94, 'W wirtualnym świecie przeczekać apokalipsę, która nigdy nie minie!'),
		'...',
		'Masz rację, ale...',
		'Nie wiesz, jak to jest.',
		'Na początku próbowałem nawiązywać sygnał z innymi, przeczesywać okolicę w poszukiwaniu śladów ludzi, szukałem jakiegokolwiek znaku, że ktoś też ocalał.',
		'Całkowicie na próżno.',
		'Każdy ma swój limit. Ja widocznie osiągnąłem swój.',
		'Dnie spędzone na bezowocnym poszukiwaniu, a po nich samotne noce w zimnym i ciemnym bunkrze.',
		'Ciężko jest coś ciągnąć, kiedy nie ma się żadnego wsparcia.',
		'Nikogo, dla kogo możesz dalej się starać.',
		'Teraz, kiedy jestem tu całkiem sam...',
		ruriri(95, 'Halo, ziemia do nerda.'),
		ruriri(96, 'Sytuacja się zmieniła.'),
		'show character r rcpose2embarassed at center',
		ruriri(97, 'Jestem tutaj, żeby cię wspierać.'),
		ruriri(98, 'Co więcej... nie cierpię, kiedy się poddajesz.'),
		ruriri(99, 'Nie zniosę rezygnacji w twoim głosie.'),
		ruriri(100, 'Więc jeśli nie dla siebie... zrób to dla mnie.'),
		'show character r rcpose2embarassed at center',
		ruriri(101, 'Będę cię wspierać aż do samego końca.'),
		'jump epilogruriri'
	],

	epilogaloes: [
		'Rin, ja...',
		'Jeśli zniknę, zostaniesz tu zamknięta.',
		'Na zawsze uwięziona w wiecznym lecie.',
		'Nie będę w stanie cię odłączyć.',
		'Nie mógłbym tego zrobić.',
		'show character a apose1happybutsad at center',
		aloes(80, 'Podejdź do tego jak matematyk.'), // smutny uśmiech
		aloes(81, 'Niech nic nie zakłóca twojego osądu.'),
		aloes(82, 'A z pewnością nie chcę, żebym to ja miała na niego wpływ.'),
		aloes(83, 'To coś, czego potem nie można sobie wybaczyć.'),
		'To trochę ironiczne, wiesz?',
		'Wtedy też zostałem postawiony przez podobnym wyborem - pojechać w nieznane i zdobyć nowe życie albo zostać z kimś dla mnie ważnym, ryzykując jednocześnie stagnację i zaprzepaszczając perspektywy.',
		'Ironia poszła jednak o krok dalej - wtedy ten świat miał być sposobem pogodzenia obu tych opcji.',
		'Powiedziałaś wcześniej, że nie wiesz, czy zdobyłabyś się na takie poświęcenie.',
		'show character a acpose2sad at center',
		'A teraz namawiasz mnie do tego, do czego wtedy namawiała mnie Rin.',
		'I tak jak ona łzy w kącikach twoich oczu mówią mi, że tak naprawdę chciałabyś czegoś zdecydowanie innego.',
		aloes(84, 'Za mocno testujesz mój zdrowy rozsądek.'),
		aloes(85, 'On też ma swoje granice.'),
		aloes(86, 'Oczywiście, że bardzo bym chciała, żebyś ze mną został.'),
		aloes(87, 'To wszystko, co mam.'),
		'show character a acpose2crying at center',
		aloes(88, 'No i... postąpiłam jak wzorowa dziewczyna.'),
		aloes(89, 'Zakochałam się w tobie zgodnie z oczekiwaniami.'),
		aloes(90, 'Zauważyłeś, prawda?'),
		aloes(91, 'Ale to nie ma teraz znaczenia.'),
		aloes(92, 'Właśnie z tego powodu mogę być dla ciebie oparciem.'),
		aloes(93, 'I będę nim, niezależnie od twojego wyboru.'),
		aloes(94, 'Chciałabym tylko...'),
		aloes(95, '...jeśli można mieć w ogóle tego typu życzenie w tak szybko zmieniającym się świecie...'),
		aloes(96, '...żebyś go nie żałował.'),
		aloes(97, 'Nie mogłabym znieść tego, jak się zadręczasz.'),
		// w zależności od tego, co ktoś wybrał w pytaniu 5, włącza się odpowiednie zakończenie
		'Rin, ja...',
		'wait 3000',
		{'Conditional': {
				'Condition': function () {
					return this.storage('hope')
				},
				'True': 'jump epilogaloeshope',
				'False': 'jump epilogaloeshappy'
			}
		}],
	epilogaloeshappy: [
		'Zostanę.',
		'show character a apose1surprised at center',
		'Oczywiście, że zostanie tu jest jedyną sensowną opcją.',
		'Jest jeszcze tyle rzeczy, o których chciałbym z tobą porozmawiać.',
		'Tyle, o które chciałbym cię zapytać.',
		'Uwięzieni na zawsze w wiecznym lecie.',
		'Nie brzmi wcale tak źle, prawda?',
		'show character a apose1happycrying at center',
		aloes(107, 'Myślę, że brzmi jak najlepsze wakacje, jakie mogłabym sobie wyobrazić.'),
		// rysunek ze szczęśliwą i zapłakaną RinAloes
		'stop sound with fade 1',
		'wait 3000',
		'show scene black with fadeIn',
		'play music ending',
		'end'
	],
	epilogaloeshope: [
		aloes(98, 'W porządku... rozumiem.'),
		'show scene black with fadeIn',
		'stop sound with fade 3',
		'wait 3000',
		'show character a acpose2sad at center with fadeIn',
		aloes(99, 'Spędziliśmy razem jeszcze kilka dni, jednak miałam wrażenie, że jego głowę zaprzątają inne myśli.'),
		aloes(100, 'W końcu, 58 dnia od czasu naszego poznania się, nie przyszedł.'),
		aloes(101, 'Nie mam mu tego za złe, nie czuję się zdradzona ani nic z tych rzeczy - w końcu sama dałam mu ten wybór.'),
		'show character a apose1happybutsad at center',
		aloes(102, 'Staram się myśleć o tym pozytywnie.'),
		aloes(103, 'Z pewnością jest teraz w bezpiecznym miejscu, najedzony i ogrzany.'),
		aloes(104, 'To wszystko, czego pragnę.'),
		aloes(105, 'A kiedy będzie mnie potrzebować, wróci...'),
		aloes(106, '... prawda?'),
		// jakiś rysunek ze smutną RinAloes
		'end'
	],

	epilogruriri: [
		'Rin, ja...',
		'show character r rcpose2distressed at center',
		'Jeśli zniknę, zostaniesz tu zamknięta.',
		'Na zawsze uwięziona w wiecznym lecie.',
		'Nie będę w stanie cię odłączyć.',
		'Nie mógłbym tego zrobić.',
		ruriri(102, 'Podejdź do tego jak matematyk.'), // smutny uśmiech
		ruriri(103, 'Kierując się wyłącznie logiką, a nie emocjami, jak wiele razy próbowałeś mnie nauczyć.'),
		'show character r rcpose2sad at center',
		ruriri(104, 'A z pewnością nie zostawaj dla mnie.'),
		ruriri(105, 'Nie wybaczyłabym sobie tego.'),
		'To trochę ironiczne, wiesz?',
		'Wtedy też zostałem postawiony przez podobnym wyborem - pojechać w nieznane i zdobyć nowe życie albo zostać z kimś dla mnie ważnym, ryzykując jednocześnie stagnację i zaprzepaszczając perspektywy.',
		'Ironia poszła jednak o krok dalej - wtedy ten świat miał być sposobem pogodzenia obu tych opcji.',
		'Powiedziałaś wcześniej, że nie wiesz, czy zdobyłabyś się na takie poświęcenie.',
		'A teraz namawiasz mnie do tego, do czego wtedy namawiała mnie Rin.',
		'I tak jak ona łzy w kącikach twoich oczu mówią mi, że tak naprawdę chciałabyś czegoś zdecydowanie innego.',
		ruriri(106, 'To tylko alergia! Musiałeś mnie tak dokładnie odtwarzać?!'), // stara się nie płakać
		'show character r rcpose2crying at center',
		ruriri(107, 'Oczywiście, że bardzo bym chciała, żebyś ze mną został.'),
		ruriri(108, 'Jesteś bezkonkurencyjnie najfajniejszym człowiekiem w tym świecie, nie żeby poprzeczka była postawiona wysoko.'),
		ruriri(109, 'Ale nawet gdybyśmy nie byli tu sami...'),
		ruriri(110, '...myślę, że naprawdę cię lubię.'),
		ruriri(111, 'Właśnie dlatego nie mogę cię zatrzymać.'),
		ruriri(112, 'Zaakceptuję, cokolwiek wybierzesz.'),
		ruriri(113, 'Chciałabym tylko...'),
		ruriri(114, '...jeśli można mieć w ogóle takie życzenie w tym świecie...'),
		ruriri(115, '...żebyś go nie żałował.'),
		ruriri(116, 'Nie mogłabym znieść tego, jak się zadręczasz.'),
		// w zależności od tego, co ktoś wybrał w pytaniu 5, włącza się odpowiednie zakończenie
		'Rin, ja...',
		'wait 3000',
		//czarny ekran
		ruriri(117, 'No już, decyduj się, nie mam na to całego dnia!'), // jakiś uśmiech przez płacz czy Coś
		'wait 3000',
		{'Conditional': {
				'Condition': function () {
					return this.storage('hope')
				},
				'True': 'jump epilogruririhope',
				'False': 'jump epilogruririhappy'
				}
		},
		],
	epilogruririhappy: [
		'Zostanę.',
		'show character r rcpose2distressed at center',
		'Oczywiście, że zostanie tu jest jedyną sensowną opcją.',
		'Przecież nie mógłbym cię tu zostawić, całkiem samej.',
		'I to po tym, jak sam cię stworzyłem.',
		'Poza tym jeśli zniknę, nie będzie nikogo, na kogo mogłabyś się złościć.',
		'show character r rcpose2embarassed at center',
		'Nie mogę narazić świata na takie ryzyko.',
		ruriri(118, 'Hmpf!'),
		ruriri(119, 'Nie myśl, że jesteś jakimś bohaterem czy coś.'),
		ruriri(120, 'To ja się tu poświęcam, spędzając z tobą cały ten czas!'),
		ruriri(121, 'A jedyne, co dostaję w zamian, to obelgi!'),
		'Składam swoje najszczersze przeprosiny i proszę o wybaczenie.',
		'show character r rcpose2embarassed2 at center',
		ruriri(122, 'Tak łatwo ci się nie upiecze!'),
		ruriri(123, 'Za karę musisz upewnić się, że na czas zdążysz przenieść się do tego świata.'),
		ruriri(124, 'Żeby już zawsze móc dotrzymywać mi towarzystwa!'),
		'Zrobię co w mojej mocy, księżniczko.',
		'show character r rpose1happy at center',
		'wait 4500',
		// rysunek końcowy Rin z blushem?
		'stop sound with fade 1',
		'show scene black with fadeIn',
		'play music ending',
		'end'
	],
	epilogruririhope: [
		ruriri(125, 'Lepiej, żeby to było tego warte, głupku.'), // płacze etc.
		ruriri(126, 'Ale nie myśl, że będę za tobą tęsknić czy coś!'),
		ruriri(127, 'No... może tylko troszkę.'),
		'Rin... obiecuję, że dam z siebie wszystko, żeby znaleźć ratunek.',
		'A jeśli mi się nie uda...',
		'Wrócę do ciebie.',
		'Dobrze?',
		// rysunek z zapłakaną Rin?
		'stop sound with fade 1',
		'end'
	],
});
