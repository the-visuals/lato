#!/usr/bin/env node
const fs = require('fs');

function replaceIndexString(indexString, script) {
	let index = 1;
	let newScript = `${script}`;
	while (newScript.includes(indexString)) {
		console.log(`${indexString}: ${index}`);
		newScript = newScript.replace(indexString, index);
		index++;
	}
	return newScript;
}

fs.readFile('script.js', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	let script = data;

	// script = replaceIndexString('aloesIndex', script);
	script = replaceIndexString('ruririIndex', script);
	// script = replaceIndexString('ruririThinkIndex', script);


	fs.writeFile('script.js', script, 'utf8', function (err) {
		if (err) return console.log(err);
	});
});


// var aloesIndex = 0;
// var ruririIndex = 0;
// var hikariIndex = 0;
// var yudekoIndex = 0;
// var aloesThinkIndex = 0;
// var ruririThinkIndex = 0;
// var hikariThinkIndex = 0;
// var yudekoThinkIndex = 0;