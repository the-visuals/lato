'use strict';

// The name of your game, no spaces or special characters.
const name = 'Monogatari';

// The version of the cache, changing this will force everything to be cached
// again.
const version = '0.1.0';

const files = [

	'/',

	// General Files
	'manifest.json',

	// Engine Files
	'engine/core/monogatari.css',
	'engine/core/monogatari.js',

	// HTML Files
	'index.html',

	// Style Sheets
	'style/main.css',

	// JavaScript Files
	'js/options.js',
	'js/storage.js',
	'js/script.js',
	'js/main.js',

	// App Images
	'favicon.ico',
	'assets/icons/icon_48x48.png',
	'assets/icons/icon_60x60.png',
	'assets/icons/icon_70x70.png',
	'assets/icons/icon_76x76.png',
	'assets/icons/icon_96x96.png',
	'assets/icons/icon_120x120.png',
	'assets/icons/icon_128x128.png',
	'assets/icons/icon_150x150.png',
	'assets/icons/icon_152x152.png',
	'assets/icons/icon_167x167.png',
	'assets/icons/icon_180x180.png',
	'assets/icons/icon_192x192.png',
	'assets/icons/icon_310x150.png',
	'assets/icons/icon_310x310.png',
	'assets/icons/icon_512x512.png',

	'assets/voices/aloes_001.mp3',
	'assets/voices/aloes_021.mp3',
	'assets/voices/aloes_041.mp3',
	'assets/voices/aloes_061.mp3',
	'assets/voices/aloes_081.mp3',
	'assets/voices/aloes_101.mp3',
	'assets/voices/ruriri014.mp3',
	'assets/voices/ruriri034.mp3',
	'assets/voices/ruriri055.mp3',
	'assets/voices/ruriri077.mp3',
	'assets/voices/ruriri099.mp3',
	'assets/voices/ruriri119.mp3',
	'assets/voices/aloes_002.mp3',
	'assets/voices/aloes_022.mp3',
	'assets/voices/aloes_042.mp3',
	'assets/voices/aloes_062.mp3',
	'assets/voices/aloes_082.mp3',
	'assets/voices/aloes_102.mp3',
	'assets/voices/ruriri015.mp3',
	'assets/voices/ruriri035.mp3',
	'assets/voices/ruriri056.mp3',
	'assets/voices/ruriri078.mp3',
	'assets/voices/ruriri100.mp3',
	'assets/voices/ruriri120.mp3',
	'assets/voices/aloes_003.mp3',
	'assets/voices/aloes_023.mp3',
	'assets/voices/aloes_043.mp3',
	'assets/voices/aloes_063.mp3',
	'assets/voices/aloes_083.mp3',
	'assets/voices/aloes_103.mp3',
	'assets/voices/ruriri016.mp3',
	'assets/voices/ruriri036.mp3',
	'assets/voices/ruriri057.mp3',
	'assets/voices/ruriri080.mp3',
	'assets/voices/ruriri101.mp3',
	'assets/voices/ruriri121.mp3',
	'assets/voices/aloes_004.mp3',
	'assets/voices/aloes_024.mp3',
	'assets/voices/aloes_044.mp3',
	'assets/voices/aloes_064.mp3',
	'assets/voices/aloes_084.mp3',
	'assets/voices/aloes_104.mp3',
	'assets/voices/ruriri017.mp3',
	'assets/voices/ruriri037.mp3',
	'assets/voices/ruriri058.mp3',
	'assets/voices/ruriri081.mp3',
	'assets/voices/ruriri102.mp3',
	'assets/voices/ruriri122.mp3',
	'assets/voices/aloes_005.mp3',
	'assets/voices/aloes_025.mp3',
	'assets/voices/aloes_045.mp3',
	'assets/voices/aloes_065.mp3',
	'assets/voices/aloes_085.mp3',
	'assets/voices/aloes_105.mp3',
	'assets/voices/ruriri018.mp3',
	'assets/voices/ruriri038.mp3',
	'assets/voices/ruriri059.mp3',
	'assets/voices/ruriri082.mp3',
	'assets/voices/ruriri103.mp3',
	'assets/voices/ruriri123.mp3',
	'assets/voices/aloes_006.mp3',
	'assets/voices/aloes_026.mp3',
	'assets/voices/aloes_046.mp3',
	'assets/voices/aloes_066.mp3',
	'assets/voices/aloes_086.mp3',
	'assets/voices/aloes_106.mp3',
	'assets/voices/ruriri019.mp3',
	'assets/voices/ruriri039.mp3',
	'assets/voices/ruriri060.mp3',
	'assets/voices/ruriri083.mp3',
	'assets/voices/ruriri104.mp3',
	'assets/voices/ruriri124.mp3',
	'assets/voices/aloes_007.mp3',
	'assets/voices/aloes_027.mp3',
	'assets/voices/aloes_047.mp3',
	'assets/voices/aloes_067.mp3',
	'assets/voices/aloes_087.mp3',
	'assets/voices/aloes_107.mp3',
	'assets/voices/ruriri020.mp3',
	'assets/voices/ruriri040.mp3',
	'assets/voices/ruriri061.mp3',
	'assets/voices/ruriri084.mp3',
	'assets/voices/ruriri105.mp3',
	'assets/voices/ruriri125.mp3',
	'assets/voices/aloes_008.mp3',
	'assets/voices/aloes_028.mp3',
	'assets/voices/aloes_048.mp3',
	'assets/voices/aloes_068.mp3',
	'assets/voices/aloes_088.mp3',
	'assets/voices/ruriri001.mp3',
	'assets/voices/ruriri021.mp3',
	'assets/voices/ruriri041.mp3',
	'assets/voices/ruriri062.mp3',
	'assets/voices/ruriri085.mp3',
	'assets/voices/ruriri106.mp3',
	'assets/voices/ruriri126.mp3',
	'assets/voices/aloes_009.mp3',
	'assets/voices/aloes_029.mp3',
	'assets/voices/aloes_049.mp3',
	'assets/voices/aloes_069.mp3',
	'assets/voices/aloes_089.mp3',
	'assets/voices/ruriri002.mp3',
	'assets/voices/ruriri022.mp3',
	'assets/voices/ruriri042.mp3',
	'assets/voices/ruriri063.mp3',
	'assets/voices/ruriri086.mp3',
	'assets/voices/ruriri107.mp3',
	'assets/voices/ruriri127.mp3',
	'assets/voices/aloes_010.mp3',
	'assets/voices/aloes_030.mp3',
	'assets/voices/aloes_050.mp3',
	'assets/voices/aloes_070.mp3',
	'assets/voices/aloes_090.mp3',
	'assets/voices/ruriri003.mp3',
	'assets/voices/ruriri023.mp3',
	'assets/voices/ruriri043.mp3',
	'assets/voices/ruriri064.mp3',
	'assets/voices/ruriri087.mp3',
	'assets/voices/ruriri108.mp3',
	'assets/voices/ruriri128.mp3',
	'assets/voices/aloes_011.mp3',
	'assets/voices/aloes_031.mp3',
	'assets/voices/aloes_051.mp3',
	'assets/voices/aloes_071.mp3',
	'assets/voices/aloes_091.mp3',
	'assets/voices/ruriri004.mp3',
	'assets/voices/ruriri024.mp3',
	'assets/voices/ruriri045.mp3',
	'assets/voices/ruriri065.mp3',
	'assets/voices/ruriri088.mp3',
	'assets/voices/ruriri109.mp3',
	'assets/voices/ruriri129.mp3',
	'assets/voices/aloes_012.mp3',
	'assets/voices/aloes_032.mp3',
	'assets/voices/aloes_052.mp3',
	'assets/voices/aloes_072.mp3',
	'assets/voices/aloes_092.mp3',
	'assets/voices/ruriri005.mp3',
	'assets/voices/ruriri025.mp3',
	'assets/voices/ruriri046.mp3',
	'assets/voices/ruriri066.mp3',
	'assets/voices/ruriri090.mp3',
	'assets/voices/ruriri110.mp3',
	'assets/voices/ruriri130.mp3',
	'assets/voices/aloes_013.mp3',
	'assets/voices/aloes_033.mp3',
	'assets/voices/aloes_053.mp3',
	'assets/voices/aloes_073.mp3',
	'assets/voices/aloes_093.mp3',
	'assets/voices/ruriri006.mp3',
	'assets/voices/ruriri026.mp3',
	'assets/voices/ruriri047.mp3',
	'assets/voices/ruriri067.mp3',
	'assets/voices/ruriri091.mp3',
	'assets/voices/ruriri111.mp3',
	'assets/voices/aloes_014.mp3',
	'assets/voices/aloes_034.mp3',
	'assets/voices/aloes_054.mp3',
	'assets/voices/aloes_074.mp3',
	'assets/voices/aloes_094.mp3',
	'assets/voices/ruriri007.mp3',
	'assets/voices/ruriri027.mp3',
	'assets/voices/ruriri048.mp3',
	'assets/voices/ruriri068.mp3',
	'assets/voices/ruriri092.mp3',
	'assets/voices/ruriri112.mp3',
	'assets/voices/aloes_015.mp3',
	'assets/voices/aloes_035.mp3',
	'assets/voices/aloes_055.mp3',
	'assets/voices/aloes_075.mp3',
	'assets/voices/aloes_095.mp3',
	'assets/voices/ruriri008.mp3',
	'assets/voices/ruriri028.mp3',
	'assets/voices/ruriri049.mp3',
	'assets/voices/ruriri069.mp3',
	'assets/voices/ruriri093.mp3',
	'assets/voices/ruriri113.mp3',
	'assets/voices/aloes_016.mp3',
	'assets/voices/aloes_036.mp3',
	'assets/voices/aloes_056.mp3',
	'assets/voices/aloes_076.mp3',
	'assets/voices/aloes_096.mp3',
	'assets/voices/ruriri009.mp3',
	'assets/voices/ruriri029.mp3',
	'assets/voices/ruriri050.mp3',
	'assets/voices/ruriri070.mp3',
	'assets/voices/ruriri094.mp3',
	'assets/voices/ruriri114.mp3',
	'assets/voices/aloes_017.mp3',
	'assets/voices/aloes_037.mp3',
	'assets/voices/aloes_057.mp3',
	'assets/voices/aloes_077.mp3',
	'assets/voices/aloes_097.mp3',
	'assets/voices/ruriri010.mp3',
	'assets/voices/ruriri030.mp3',
	'assets/voices/ruriri051.mp3',
	'assets/voices/ruriri071.mp3',
	'assets/voices/ruriri095.mp3',
	'assets/voices/ruriri115.mp3',
	'assets/voices/aloes_018.mp3',
	'assets/voices/aloes_038.mp3',
	'assets/voices/aloes_058.mp3',
	'assets/voices/aloes_078.mp3',
	'assets/voices/aloes_098.mp3',
	'assets/voices/ruriri011.mp3',
	'assets/voices/ruriri031.mp3',
	'assets/voices/ruriri052.mp3',
	'assets/voices/ruriri073.mp3',
	'assets/voices/ruriri096.mp3',
	'assets/voices/ruriri116.mp3',
	'assets/voices/aloes_019.mp3',
	'assets/voices/aloes_039.mp3',
	'assets/voices/aloes_059.mp3',
	'assets/voices/aloes_079.mp3',
	'assets/voices/aloes_099.mp3',
	'assets/voices/ruriri012.mp3',
	'assets/voices/ruriri032.mp3',
	'assets/voices/ruriri053.mp3',
	'assets/voices/ruriri075.mp3',
	'assets/voices/ruriri097.mp3',
	'assets/voices/ruriri117.mp3',
	'assets/voices/aloes_020.mp3',
	'assets/voices/aloes_040.mp3',
	'assets/voices/aloes_060.mp3',
	'assets/voices/aloes_080.mp3',
	'assets/voices/aloes_100.mp3',
	'assets/voices/ruriri013.mp3',
	'assets/voices/ruriri033.mp3',
	'assets/voices/ruriri054.mp3',
	'assets/voices/ruriri076.mp3',
	'assets/voices/ruriri098.mp3',
	'assets/voices/ruriri118.mp3'
];

self.addEventListener ('install', (event) => {
	self.skipWaiting ();
	event.waitUntil (
		caches.open (`${name}-v${version}`).then ((cache) => {
			return cache.addAll (files);
		})
	);
});

self.addEventListener ('activate', (event) => {
	event.waitUntil (
		caches.keys ().then ((keyList) => {
			return Promise.all (keyList.map ((key) => {
				if (key !== `${name}-v${version}`) {
					return caches.delete (key);
				}
			}));
		})
	);
	return self.clients.claim ();
});

self.addEventListener ('fetch', (event) => {

	if (event.request.method !== 'GET') {
		return;
	}

	event.respondWith (
		caches.match (event.request).then ((cached) => {
			function fetchedFromNetwork (response) {
				const cacheCopy = response.clone ();

				caches.open (`${name}-v${version}`).then (function add (cache) {
					cache.put (event.request, cacheCopy);
				});
				return response;
			}

			function unableToResolve () {
				return new Response (`
					<!DOCTYPE html><html lang=en><title>Bad Request</title><meta charset=UTF-8><meta content="width=device-width,initial-scale=1"name=viewport><style>body,html{width:100%;height:100%}body{text-align:center;color:#545454;margin:0;display:flex;justify-content:center;align-items:center;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Fira Sans","Droid Sans","Helvetica Neue",sans-serif}h1,h2{font-weight:lighter}h1{font-size:4em}h2{font-size:2em}</style><div><h1>Service Unavailable</h1><h2>Sorry, the server is currently unavailable or under maintenance, try again later.</h2></div>
				`, {
					status: 503,
					statusText: 'Service Unavailable',
					headers: new Headers ({
						'Content-Type': 'text/html'
					})
				});
			}

			const networked = fetch (event.request)
				.then (fetchedFromNetwork, unableToResolve)
				.catch (unableToResolve);

			return cached || networked;
		})
	);
});